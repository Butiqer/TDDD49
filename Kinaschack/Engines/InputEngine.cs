﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Kinaschack.Engines
{
    class InputEngine
    {
        private GameParts.Board Board { get; set; }

        private MouseState PastMouseState;
        private List<GameParts.GameObject> OverObjects;
        private GameParts.GameObject SelectedObject;

        public InputEngine(GameParts.Board board)
        {
            this.Board = board;
            this.PastMouseState = new MouseState();
            this.OverObjects = new List<GameParts.GameObject>();
        }

        public void HandleInput(float dt)
        {
            var mouseState = Mouse.GetState();

            if(this.SelectedObject == null)
            {
                foreach (var piece in this.Board.CurrentPlayer.Pieces)
                {
                    if (piece.ContainsPoint(mouseState.Position))
                    {
                        if (!OverObjects.Contains(piece))
                        {
                            OverObjects.Add(piece);
                            this.Board.MouseEnter(piece);
                        }
                        else if (PastMouseState.LeftButton == ButtonState.Pressed && mouseState.LeftButton == ButtonState.Released)
                        {
                            this.SelectedObject = piece;
                        }
                    }
                    else if (OverObjects.Contains(piece))
                    {
                        OverObjects.Remove(piece);
                        this.Board.MouseLeave(piece);
                    }
                }
            }
            else
            {
                foreach (var hole in this.Board.Holes)
                {
                    if (hole.ContainsPoint(mouseState.Position))
                    {
                        if (!OverObjects.Contains(hole))
                        {
                            OverObjects.Add(hole);
                            this.Board.MouseEnter(hole);
                        }
                        else if (PastMouseState.LeftButton == ButtonState.Pressed && mouseState.LeftButton == ButtonState.Released)
                        {
                            if (this.SelectedObject != null)
                            {
                                this.Board.RequestMove(this.Board.CurrentPlayer, (GameParts.PlayerPiece)this.SelectedObject, hole.HexCoord);

                                OverObjects.Remove(this.SelectedObject);
                                this.Board.MouseLeave((GameParts.PlayerPiece)this.SelectedObject);
                                this.SelectedObject = null;
                            }
                            OverObjects.Remove(hole);
                            this.Board.MouseLeave(hole);
                        }
                    }
                    else if (OverObjects.Contains(hole))
                    {
                        OverObjects.Remove(hole);
                        this.Board.MouseLeave(hole);
                    }
                }
            }

            this.PastMouseState = mouseState;
        }
    }
}
