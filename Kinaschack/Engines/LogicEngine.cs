﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kinaschack.Engines
{
    class LogicEngine
    {
        private GameParts.Board Board { get; set; }
        private RuleEngine RuleEngine { get; set; }

        private Player.Player Player;
        private GameParts.PlayerPiece Piece;
        private GameParts.Hex Destination;

        public List<Player.Player> ResultsTable { get; set; }

        public delegate void ResultsUpdatedDelegate(List<Player.Player> ResultsTable);
        public event ResultsUpdatedDelegate OnResultsUpdated;

        bool AIRequestedMove = false;
        float AIDelay = .05f;
        float AIDelayCounter = 0;

        public LogicEngine(GameParts.Board board, RuleEngine ruleEngine)
        {
            this.Board = board;
            this.Board.OnRequestMove += RequestsMove;
            this.RuleEngine = ruleEngine;
            this.ResultsTable = new List<Player.Player>();
        }

        public bool Update(float dt)
        {
            if (!this.Board.Players.Exists(x => !x.IsDone))
                return false;

            if (this.AIRequestedMove)
            {
                if(this.AIDelayCounter >= this.AIDelay)
                {
                    this.AIRequestedMove = false;
                    this.AIDelayCounter = 0;
                    this.UpdateResults();
                    this.Board.NextPlayersTurn();
                    while (this.RuleEngine.PlayerInEndHoles(this.Board.CurrentPlayer))
                    {
                        this.Board.CurrentPlayer.IsDone = true;
                        if (!this.Board.Players.Exists(x => !x.IsDone))
                            return false;

                        this.Board.NextPlayersTurn();
                    }
                    this.Board.CurrentPlayer.PlayTurn();
                        
                }
                this.AIDelayCounter += dt;
            }
            return true;
        }

        public void RequestsMove(Player.Player player, GameParts.PlayerPiece piece, GameParts.Hex destination)
        {
            if (!(player is Player.AIPlayer))
            {
                if (this.RuleEngine.PlayerCanMoveTo(piece, destination))
                {
                    piece.HexCoord = destination;
                    this.UpdateResults();
                    this.Board.NextPlayersTurn();
                    while (this.RuleEngine.PlayerInEndHoles(this.Board.CurrentPlayer))
                    {
                        this.Board.CurrentPlayer.IsDone = true;
                        if (!this.Board.Players.Exists(x => !x.IsDone))
                            break;
                        this.Board.NextPlayersTurn();
                    }
                    this.Board.CurrentPlayer.PlayTurn();
                }
            }
            else
            {
                this.AIRequestedMove = true;
                this.Player = player;
                this.Piece = piece;
                this.Destination = destination;
                this.Piece.HexCoord = this.Destination;
            }
        }

        private void UpdateResults()
        {
            if (this.RuleEngine.PlayerInEndHoles(this.Board.CurrentPlayer) && 
               !this.ResultsTable.Contains(this.Board.CurrentPlayer))
            {
                this.ResultsTable.Add(this.Board.CurrentPlayer);
                this.OnResultsUpdated(this.ResultsTable);
            }
        }
    }
}
