﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Kinaschack.Engines
{
    class RenderEngine
    {
        private List<GameParts.GameObject> GameObjects { get; set; }
        private SpriteBatch SpriteBatch { get; set; }

        public RenderEngine()
        {
            this.GameObjects = new List<GameParts.GameObject>();
        }

        public void LoadContent(SpriteBatch spriteBatch)
        {
            this.SpriteBatch = spriteBatch;
        }

        public void UnloadContent()
        {
            
        }

        public void Add(GameParts.GameObject gameObject)
        {
            this.GameObjects.Add(gameObject);
        }

        public void Add(IEnumerable<GameParts.GameObject> gameObjects)
        {
            this.GameObjects.AddRange(gameObjects);
        }

        public void Remove(GameParts.GameObject gameObject)
        {
            this.GameObjects.Remove(gameObject);
        }

        public void Remove(IEnumerable<GameParts.GameObject> gameObjects)
        {
            foreach(var go in gameObjects)
                this.GameObjects.Remove(go);
        }

        public void Clear()
        {
            this.GameObjects.Clear();
        }

        public void Render(float dt)
        {
            this.SortOjects();

            this.SpriteBatch.Begin();

            foreach(var gameObject in this.GameObjects)
            { 
                if(gameObject.Texture != null)
                    this.SpriteBatch.Draw(gameObject.Texture, new Rectangle(gameObject.Position.ToPoint(), gameObject.Size.ToPoint()), gameObject.Tint);
            }
            
            this.SpriteBatch.End();
        }

        private void SortOjects()
        {
            this.GameObjects.Sort((x, y) => x.Layer.CompareTo(y.Layer));
        }
    }
}
