﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kinaschack.Engines
{
    class RuleEngine
    {
        public GameParts.Board Board { get; set; }
        public RuleEngine(GameParts.Board board)
        {
            this.Board = board;
            this.Board.RuleEngine = this;
        }

        public bool PlayerCanMoveTo(GameParts.PlayerPiece playerPiece, GameParts.Hex destination)
        {
            bool canMove = true;

            if (this.Board.GetPlayerPieceInHex(destination) != null)
                canMove = false;

            if (GameParts.Hex.Distance(playerPiece.HexCoord, destination) == 2)
            {
                if(playerPiece.HexCoord.Y - destination.Y != 0 && playerPiece.HexCoord.Y - destination.Y != 0 && playerPiece.HexCoord.Z - destination.Z == 0)
                {
                    GameParts.Hex betweenHex = new GameParts.Hex(playerPiece.HexCoord.X - (playerPiece.HexCoord.X - destination.X) / 2, playerPiece.HexCoord.Y - (playerPiece.HexCoord.Y - destination.Y) / 2, playerPiece.HexCoord.Z);
                    if (this.Board.GetPlayerPieceInHex(betweenHex) == null)
                        canMove = false;
                }
                else if(playerPiece.HexCoord.X - destination.X != 0)
                {
                    GameParts.Hex betweenHex = new GameParts.Hex(playerPiece.HexCoord.X - (playerPiece.HexCoord.X - destination.X) / 2, playerPiece.HexCoord.Y, playerPiece.HexCoord.Z - (playerPiece.HexCoord.Z - destination.Z) / 2);
                    if (this.Board.GetPlayerPieceInHex(betweenHex) == null)
                        canMove = false;
                }
                else if (playerPiece.HexCoord.Y - destination.Y != 0)
                {
                    GameParts.Hex betweenHex = new GameParts.Hex(playerPiece.HexCoord.X, playerPiece.HexCoord.Y - (playerPiece.HexCoord.Y - destination.Y) / 2, playerPiece.HexCoord.Z - (playerPiece.HexCoord.Z - destination.Z) / 2);
                    if (this.Board.GetPlayerPieceInHex(betweenHex) == null)
                        canMove = false;
                }
            }
            else if (GameParts.Hex.Distance(playerPiece.HexCoord, destination) > 1)
            {
                canMove = false;
            }

            if (!this.Board.ContainsHoleAt(destination))
                canMove = false;
                

            return canMove;
        }

        public bool PlayerInEndHoles(Player.Player player)
        {
            foreach(var piece in player.Pieces)
            {
                if (!player.GoalPositions.Exists(x => x == piece.HexCoord))
                    return false;
            }
            return true;
        }
    }
}
