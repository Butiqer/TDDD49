﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace Kinaschack
{
    public class GameBase : Game
    {
        private SpriteBatch SpriteBatch;
        private GraphicsDeviceManager Graphics;

        private Scenes.IScene CurrentScene;
        private Scenes.IScene NextScene;

        public GameBase()
        {
            this.IsMouseVisible = true;
            Content.RootDirectory = "Content";
            this.Graphics = new GraphicsDeviceManager(this);
            this.Graphics.PreferredBackBufferHeight = 900;
            this.Graphics.PreferredBackBufferWidth = 900;

            CurrentScene = new Scenes.GameScene();
        }
        
        
        protected override void Initialize()
        {
            base.Initialize();

            if(!this.CurrentScene.InitFromSavedData())
                this.CurrentScene.Initialize();
        }
        
        protected override void LoadContent()
        {
            this.SpriteBatch = new SpriteBatch(this.GraphicsDevice);
            this.CurrentScene.LoadContent(this.SpriteBatch);
        }
        
        protected override void UnloadContent()
        {
        }
        
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            var dt = (float)gameTime.ElapsedGameTime.TotalSeconds;

            if(!this.CurrentScene.Update(dt))
            {
                this.CurrentScene = this.NextScene;
                this.CurrentScene.LoadContent(this.SpriteBatch);
                if (!this.CurrentScene.InitFromSavedData())
                    this.CurrentScene.Initialize();
            }

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            var dt = (float)gameTime.ElapsedGameTime.TotalSeconds;

            this.CurrentScene.Render(dt);

            base.Draw(gameTime);
        }
    }
}
