﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Kinaschack.Player;

namespace Kinaschack.GameParts
{
    class Board
    {
        public Engines.RuleEngine RuleEngine { get; set; }
        public List<HexGameObject> Holes { get; private set; }
        public List<Player.Player> Players { get; private set; }

        public int CurrentPlayerId { get; set; }
        public Player.Player CurrentPlayer { get { return Players[CurrentPlayerId]; }}

        public delegate void RequestsMoveDelegate(Player.Player player, PlayerPiece piece, Hex destination);
        public event RequestsMoveDelegate OnRequestMove;

        public bool IsSaved { get; set; }

        private Texture2D HexTexture { get; set; }

        public Board()
        {
            this.Holes = new List<HexGameObject>();
            this.Players = new List<Player.Player>();
        }

        public Board(List<Player.Player> players)
        {
            this.Holes = new List<HexGameObject>();
            this.Players = players;

            foreach(var player in this.Players)
            {
                player.Board = this;
            }
        }

        public void LoadContent(SpriteBatch spriteBatch)
        {
            this.HexTexture = this.LoadTextureFromFile(spriteBatch, "Content/hex2.png");
            foreach (var player in this.Players)
            {
                player.Texture = this.LoadTextureFromFile(spriteBatch, "Content/player" + player.Id + ".png");
            }
        }

        public Texture2D LoadTextureFromFile(SpriteBatch spriteBatch, string filename)
        {            
            using (System.IO.Stream stream = TitleContainer.OpenStream(filename))
                return Texture2D.FromStream(spriteBatch.GraphicsDevice, stream);
        }

        public void InitializeBoard()
        {
            int x = 10, y = 3;
            this.CreateHexagon(x, y);
            this.InitializeTriangles(x + 1, y + 1);
        }

        private void InitializeTriangles(int x, int y)
        {
            this.CreateRightTriangle(x, y);
            this.CreateRightTriangle(x, y - 9);
            this.CreateRightTriangle(x - 9, y);
            this.CreateLeftTriangle(x - 2, y + 7);
            this.CreateLeftTriangle(x - 2, y - 2);
            this.CreateLeftTriangle(x + 7, y - 2);
        }

        private void CreateRightTriangle(int x, int y)
        {
            int map_size = 3;
            for (int q = 0; q <= map_size; q++)
            {
                for (int r = map_size - q; r <= map_size; r++)
                {
                    Holes.Add(new HexGameObject(new Vector2(r + x, q + y), new Vector2(1f, 1f), HexTexture));
                }
            }
        }

        public void RequestMove(Player.Player currentPlayer, PlayerPiece selectedObject, Hex hexCoord)
        {   
            this.OnRequestMove(currentPlayer, selectedObject, hexCoord);
            this.IsSaved = false;
        }

        private void CreateLeftTriangle(int x, int y)
        {
            int map_size = 3;
            for (int q = 0; q <= map_size; q++)
            {
                for (int r = map_size - q; r <= map_size; r++)
                {
                    Holes.Add(new HexGameObject(new Vector2(-r + x, -q + y), new Vector2(1f, 1f), HexTexture));
                }
            }
        }

        private void CreateHexagon(int x, int y)
        {
            int map_radius = 4;
            for (int q = -map_radius; q <= map_radius; q++)
            {
                int r1 = Math.Max(-map_radius, -q - map_radius);
                int r2 = Math.Min(map_radius, -q + map_radius);
                for (int r = r1; r <= r2; r++)
                {
                    Holes.Add(new HexGameObject(new Vector2(r + x, q + y), new Vector2(1f, 1f), HexTexture));
                }
            }
        }

        public void MouseEnter(HexGameObject go)
        {
            go.Tint = Color.Green;
        }

        public void MouseLeave(HexGameObject go)
        {
            go.Tint = Color.White;
        }

        public void NextPlayersTurn()
        {
            if (CurrentPlayerId + 1 == this.Players.Count)
                CurrentPlayerId = 0;
            else
                CurrentPlayerId++;

            this.IsSaved = false;
        }

        public PlayerPiece GetPlayerPieceInHex(Hex position)
        {
            foreach (var player in this.Players)
            {
                foreach (var piece in player.Pieces)
                {
                    if (piece.HexCoord == position)
                        return piece;
                }
            }
            return null;
        }

        public bool ContainsHoleAt(Hex position)
        {
            foreach(var hole in this.Holes)
            {
                if (hole.HexCoord == position)
                    return true;
            }
            return false;
        }
    }
}
