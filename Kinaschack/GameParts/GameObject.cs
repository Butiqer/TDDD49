﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Kinaschack.GameParts
{
    class GameObject
    {
        public virtual Texture2D Texture { get; set; }
        public virtual Vector2 Position { get; set; }
        public Vector2 Scale { get; set; }
        public Color Tint { get; set; }
        public Vector2 Size
        {
            get
            {
                if (this.Texture == null)
                    return this.Scale;
                return new Vector2(this.Texture.Width * this.Scale.X, this.Texture.Height * this.Scale.Y);
            }
        }

        public int Layer { get; set; }
        

        public GameObject()
        {
            this.Position = new Vector2();
            this.Scale = Vector2.One;
            this.Tint = Color.White;
        }

        public GameObject(Vector2 position)
        {
            this.Position = position;
            this.Scale = Vector2.One;
            this.Tint = Color.White;
        }

        public GameObject(Vector2 position, Vector2 scale, Texture2D texture)
        {
            this.Position = position;
            this.Texture = texture;
            this.Scale = scale;
            this.Tint = Color.White;
        }

        public virtual bool ContainsPoint(Vector2 point)
        {
            return this.ContainsPoint(point.ToPoint());
        }

        public virtual bool ContainsPoint(Point point)
        {
            var objectRect = this.Texture.Bounds;
            objectRect.Location += this.Position.ToPoint();
            objectRect.Size *= new Vector2(objectRect.Size.X * this.Scale.X, objectRect.Size.Y * this.Scale.Y).ToPoint();
            return objectRect.Contains(point);
        }
    }
}
