﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Kinaschack.GameParts
{
    class Hex
    {
        public float X { get; set; }
        public float Y { get; set; }
        public float Z { get; set; }
        public int Size { get; set; }

        public float Width
        {
            get
            {
                return this.Height * 3f / 4f;
            }
        }

        public float Height
        {
            get
            {
                return this.Size * 2;
            }
        }

        public Hex()
        {
            this.X = 0;
            this.Y = 0;
            this.Z = 0;
            this.Size = 1;
        }

        public Hex(float q, float r)
        {
            this.X = q;
            this.Z = r;
            this.Y = -this.X - this.Z;
            this.Size = 1;
        }

        public Hex(float x, float y, float z)
        {
            this.X = x;
            this.Y = y;
            this.Z = z;
            this.Size = 1;
        }
        

        public bool Contains(int x, int y)
        {
            float q = x * 2 / 3 / Size;
            float r = (float)((-x / 3 + Math.Sqrt(3) / 3 * y) / Size);
            Hex h = RoundToNearest(new Hex(q, r));

            return h.X == this.X && h.Y == this.Y && h.Z == this.Z;
        }

        public Vector2 ToPixelPosition()
        {
            float x = Size * 3 / 2 * X;
            float y = (float)(Size * Math.Sqrt(3) * (Z + X / 2));

            return new Vector2(x - Width*(1f/8f), y - Height * (1f / 4f));
        }

        public Vector2 Get2DSize()
        {
            return new Vector2(this.Width, this.Height);
        }

        public void SetPosition(float x, float y)
        {
            this.X = x;
            this.Z = y;
            this.Y = -this.X - this.Z;
        }

        private Hex RoundToNearest(Hex hex)
        {
            int rx = (int)Math.Round(hex.X);
            int ry = (int)Math.Round(hex.Y);
            int rz = (int)Math.Round(hex.Z);

            int x_diff = (int)Math.Abs(rx - hex.X);
            int y_diff = (int)Math.Abs(ry - hex.Y);
            int z_diff = (int)Math.Abs(rz - hex.Z);

            if (x_diff > y_diff && x_diff > z_diff)
            {
                rx = -ry - rz;
            }
            else if (y_diff > z_diff)
            {
                ry = -rx - rz;
            }
            else
            {
                rz = -rx - ry;
            }
            return new Hex(rx, ry, rz);
        }

        public static int Distance(Hex c1, Hex c2)
        {
            return (int)((Math.Abs(c1.X - c2.X) + Math.Abs(c1.Y - c2.Y) + Math.Abs(c1.Z - c2.Z)) / 2);
        }

        public static bool operator ==(Hex a, Hex b)
        {
            if (System.Object.ReferenceEquals(a, b))
            {
                return true;
            }
            
            if ((object)a == null || (object)b == null)
            {
                return false;
            }
            
            return a.X == b.X && a.Y == b.Y && a.Z == b.Z;
        }

        public static bool operator !=(Hex a, Hex b)
        {
            return !(a == b);
        }

        public static Hex operator +(Hex a, Hex b)
        {
            Hex newHex = new Hex(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
            newHex.Size = a.Size;
            return newHex;
        }

        public static Hex operator *(Hex a, int b)
        {
            Hex newHex = new Hex(a.X * b, a.Y * b, a.Z * b);
            newHex.Size = a.Size;
            return newHex;
        }
    }
}
