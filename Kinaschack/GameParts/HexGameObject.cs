﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Kinaschack.GameParts
{
    class HexGameObject : GameObject
    {
        public Hex HexCoord { get; set; }
        public override Vector2 Position
        {
            get
            {
                return this.HexCoord.ToPixelPosition();
            }
            set
            {
                if (this.HexCoord == null)
                    this.HexCoord = new Hex();
                this.HexCoord.SetPosition(value.X, value.Y);
            }
        }

        public override Texture2D Texture
        {
            get { return base.Texture; }
            set
            {
                base.Texture = value;
                this.HexCoord.Size = (int)(this.Texture.Width / 2f * this.Scale.Y);
            }
        }


        public HexGameObject()
        {
            this.HexCoord = new Hex();
            this.Position = new Vector2();
            this.Scale = Vector2.One;
        }

        public HexGameObject(Vector2 position)
        {
            this.HexCoord = new Hex();
            this.Position = position;
            this.Scale = Vector2.One;
        }

        public HexGameObject(Vector2 position, Vector2 scale, Texture2D texture)
        {
            this.HexCoord = new Hex();
            this.Position = position;
            this.Texture = texture;
            this.Scale = scale;
            this.HexCoord.Size = (int)(texture.Width / 2f * scale.Y);
        }

        public override bool ContainsPoint(Point point)
        {
            return this.HexCoord.Contains(point.X, point.Y);
        }

    }
}
