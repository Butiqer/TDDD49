﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Kinaschack.GameParts
{
    class PlayerPiece : HexGameObject
    {
        public Player.Player Player {get; private set;}

        public PlayerPiece(Player.Player player) : base()
        {
            this.Player = player;
        }

        public PlayerPiece(Player.Player player, Vector2 position) : base(position)
        {
            this.Player = player;
        }

        public PlayerPiece(Player.Player player, Vector2 position, Vector2 scale, Texture2D texture) : base(position, scale, texture)
        {
            this.Player = player;
        }
    }
}
