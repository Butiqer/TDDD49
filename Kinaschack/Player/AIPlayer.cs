﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kinaschack.Player
{
    class AIPlayer : Player
    {
        public List<GameParts.PlayerPiece> PiecesInPlace { get; set; }
        public int TargetPiece { get; set; }
        private struct Move
        {
            public GameParts.Hex destination;
            public GameParts.PlayerPiece piece;
            public int value;
        }

        public AIPlayer() : base()
        {
            this.PiecesInPlace = new List<GameParts.PlayerPiece>();
        }

        public AIPlayer(int targetPiece) : base()
        {
            this.PiecesInPlace = new List<GameParts.PlayerPiece>();
            this.TargetPiece = targetPiece;
        }

        public override void PlayTurn()
        {
            if (TargetPiece == this.GoalPositions.Count)
            {
                this.Board.NextPlayersTurn();
                return;
            }

            var bestMove = this.GetBestMove();

            if(bestMove.piece == null)
            {
                this.Board.NextPlayersTurn();
                return;
            }

            this.Board.RequestMove(this, bestMove.piece, bestMove.destination);
            if (TargetPiece < this.GoalPositions.Count && bestMove.piece.HexCoord == this.GoalPositions[TargetPiece])
            {
                GameParts.PlayerPiece piece = bestMove.piece;
                do
                {
                    if (piece.Player == this)
                    {
                        TargetPiece++;
                        this.PiecesInPlace.Add(piece);
                    }
                }
                while (TargetPiece < this.GoalPositions.Count && (piece = this.Board.GetPlayerPieceInHex(this.GoalPositions[TargetPiece])) != null && piece.Player == this);

                /*if (TargetPiece < this.GoalPositions.Count)
                    this.Board.Holes.Find(x => x.HexCoord == this.GoalPositions[TargetPiece]).Tint = Microsoft.Xna.Framework.Color.Red;*/
            }
        }

        private Move GetBestMove()
        {
            List<Move> moves = new List<Move>();
            List<GameParts.Hex> directions = new List<GameParts.Hex> {
                new GameParts.Hex(+1, -1, 0), new GameParts.Hex(+1, 0, -1), new GameParts.Hex(0, +1, -1),
                new GameParts.Hex(-1, +1, 0), new GameParts.Hex(-1, 0, +1), new GameParts.Hex(0, -1, +1)
            };

            foreach (var piece in this.Pieces.Where(x => !this.PiecesInPlace.Contains(x)))
            {
                foreach(var direction in directions)
                {
                    if (this.Board.RuleEngine.PlayerCanMoveTo(piece, piece.HexCoord + direction))
                        moves.Add(new Move() { piece = piece, destination = piece.HexCoord + direction, value = -GameParts.Hex.Distance(piece.HexCoord + direction, this.GoalPositions[TargetPiece]) + GameParts.Hex.Distance(piece.HexCoord, this.GoalPositions[0]) });
                }

                foreach (var direction in directions)
                {
                    if (this.Board.RuleEngine.PlayerCanMoveTo(piece, piece.HexCoord + direction * 2))
                        moves.Add(new Move() { piece = piece, destination = piece.HexCoord + direction * 2, value = -GameParts.Hex.Distance(piece.HexCoord + direction * 2, this.GoalPositions[TargetPiece]) + GameParts.Hex.Distance(piece.HexCoord, this.GoalPositions[0]) });
                }
            }

            return moves.Find(x => x.value == moves.Max(Y => Y.value));
        }

        private void PredictMoves()
        {
            // Haha nope
        }
    }
}
