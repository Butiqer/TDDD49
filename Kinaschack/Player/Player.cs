﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Kinaschack.Player
{
    class Player
    {
        private static int _ID = 0;
        public int Id { get; set; }

        private Texture2D _Texture;
        public Texture2D Texture
        {
            get { return _Texture; }
            set
            {
                this._Texture = value;
                foreach (var piece in this.Pieces)
                    piece.Texture = _Texture;
            }
        }
        public List<GameParts.PlayerPiece> Pieces { get; private set; }
        public List<GameParts.Hex> GoalPositions { get; private set; }
        public bool IsDone { get; set; }

        public GameParts.Board Board { get; set; }

        public Player()
        {
            this.Id = _ID++;
            this.Pieces = new List<GameParts.PlayerPiece>();
            this.GoalPositions = new List<GameParts.Hex>();
            this.InitPieces();
            this.InitGoal();
        }

        private void InitPieces()
        {
            switch (Id)
            {
                case 0:
                    this.CreateLeftTriangle(2, 4);
                    break;
                case 1:
                    this.CreateRightTriangle(18, 2);
                    break;
                case 2:
                    this.CreateRightTriangle(9, 2);
                    break;
                case 3:
                    this.CreateLeftTriangle(11, 4);
                    break;
                case 4:
                    this.CreateRightTriangle(9, 11);
                    break;
                case 5:
                    this.CreateLeftTriangle(11, -5);
                    break;
            }
            
        }

        private void CreateLeftTriangle(int x, int y)
        {
            foreach (var coord in this.GetLeftTriangleCoords(x, y))
            {
                var newPiece = new GameParts.PlayerPiece(this);
                newPiece.HexCoord = new GameParts.Hex();
                newPiece.Position = coord;
                newPiece.Scale = Vector2.One;
                newPiece.Layer = 1;
                this.Pieces.Add(newPiece);
            }
        }

        private void CreateRightTriangle(int x, int y)
        {
            foreach(var coord in this.GetRightTriangleCoords(x, y))
            {
                var newPiece = new GameParts.PlayerPiece(this);
                newPiece.HexCoord = new GameParts.Hex();
                newPiece.Position = coord;
                newPiece.Scale = Vector2.One;
                newPiece.Layer = 1;
                this.Pieces.Add(newPiece);
            }
        }

        private List<Vector2> GetLeftTriangleCoords(int x, int y)
        {
            List<Vector2> coords = new List<Vector2>();

            int map_size = 3;
            for (int q = 0; q <= map_size; q++)
            {
                for (int r = map_size - q; r <= map_size; r++)
                {
                    coords.Add(new Vector2(r + x, q + y));
                }
            }

            return coords;
        }

        private List<Vector2> GetRightTriangleCoords(int x, int y)
        {
            List<Vector2> coords = new List<Vector2>();

            int map_size = 3;
            for (int q = 0; q <= map_size; q++)
            {
                for (int r = map_size - q; r <= map_size; r++)
                {
                    coords.Add(new Vector2(-r + x, -q + y));
                }
            }

            return coords;
        }

        private void InitGoal()
        {
            switch (Id)
            {
                case 0:
                    foreach (var coord in this.GetRightTriangleCoords(18, 2).OrderBy(x => -x.X))
                        this.GoalPositions.Add(new GameParts.Hex(coord.X, coord.Y));
                    break;
                case 1:
                    foreach (var coord in this.GetLeftTriangleCoords(2, 4).OrderBy(x => x.X))
                        this.GoalPositions.Add(new GameParts.Hex(coord.X, coord.Y));
                    break;
                case 2:
                    foreach (var coord in this.GetLeftTriangleCoords(11, 4).OrderBy(x => -x.Y * x.X))
                        this.GoalPositions.Add(new GameParts.Hex(coord.X, coord.Y));
                    break;
                case 3:
                    foreach (var coord in this.GetRightTriangleCoords(9, 2).OrderBy(x => x.Y / x.X))
                        this.GoalPositions.Add(new GameParts.Hex(coord.X, coord.Y));
                    break;
                case 4:
                    foreach (var coord in this.GetLeftTriangleCoords(11, -5).OrderBy(x => x.Y))
                        this.GoalPositions.Add(new GameParts.Hex(coord.X, coord.Y));
                    break;
                case 5:
                    foreach (var coord in this.GetRightTriangleCoords(9, 11).OrderBy(x => -x.Y))
                        this.GoalPositions.Add(new GameParts.Hex(coord.X, coord.Y));
                    break;
            }
        }

        public virtual void PlayTurn()
        {

        }

        public static void ResetPlayers()
        {
            Player._ID = 0;
        }
    }
}
