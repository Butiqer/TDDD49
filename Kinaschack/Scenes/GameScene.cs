﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Xml.Linq;
using System.IO;
using System.Security.Permissions;

namespace Kinaschack.Scenes
{
    class GameScene : IScene
    {
        public GameParts.Board Board;
        public Engines.RuleEngine RuleEngine;
        public Engines.LogicEngine LogicEngine;
        public Engines.InputEngine InputEngine;
        public Engines.RenderEngine RenderEngine;

        private SpriteBatch SpriteBatch;
        private FileSystemWatcher FileSystemWatcher;

        public FileSystemWatcher SaveFileWatcher;

        private GameParts.GameObject CurrentPlayerIndicator;
        private List<GameParts.GameObject> ResultsTableObjects;

        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        public GameScene()
        {
            this.SaveFileWatcher = new FileSystemWatcher();
            this.SaveFileWatcher.Path = Environment.CurrentDirectory;
            this.SaveFileWatcher.NotifyFilter = NotifyFilters.LastWrite;
            this.SaveFileWatcher.Filter = "GameState.xml";

            this.SaveFileWatcher.Changed += new FileSystemEventHandler(OnSaveFileChanged);
            this.SaveFileWatcher.EnableRaisingEvents = true;

            this.CurrentPlayerIndicator = new GameParts.GameObject();
            this.CurrentPlayerIndicator.Position = new Vector2(10, 10);

            this.ResultsTableObjects = new List<GameParts.GameObject>();
        }

        private void OnSaveFileChanged(object source, FileSystemEventArgs e)
        {
            this.InitFromSavedData();
        }

        public void LoadContent(SpriteBatch spriteBatch)
        {
            this.SpriteBatch = spriteBatch;
        }

        public void Initialize()
        {
            Player.Player.ResetPlayers();
            this.InitBoard(new List<Player.Player>{
                new Player.AIPlayer(), new Player.AIPlayer(),
                new Player.AIPlayer(), new Player.AIPlayer(),
                new Player.AIPlayer(), new Player.AIPlayer()
            }, 0);
        }

        private void InitBoard(List<Player.Player> players, int currentPlayerId)
        {
            this.Board = new GameParts.Board(players);
            this.Board.LoadContent(this.SpriteBatch);

            this.RuleEngine = new Engines.RuleEngine(this.Board);
            this.LogicEngine = new Engines.LogicEngine(this.Board, this.RuleEngine);
            this.InputEngine = new Engines.InputEngine(this.Board);
            this.RenderEngine = new Engines.RenderEngine();
            this.RenderEngine.LoadContent(this.SpriteBatch);

            this.Board.InitializeBoard();
            this.InitGraphics();

            this.LogicEngine.OnResultsUpdated += this.UpdateResults;

            this.Board.CurrentPlayerId = currentPlayerId;

            this.CurrentPlayerIndicator.Texture = this.Board.CurrentPlayer.Texture;

            this.Board.CurrentPlayer.PlayTurn();
        }

        public void ResetGame()
        {
            this.Initialize();
            this.Board.LoadContent(this.SpriteBatch);
            this.RenderEngine.LoadContent(this.SpriteBatch);
            this.Board.InitializeBoard();
            this.InitGraphics();

            this.Board.CurrentPlayer.PlayTurn();
        }

        public bool Update(float dt)
        {
            this.InputEngine.HandleInput(dt);
            if (!this.LogicEngine.Update(dt))
            {
                this.ResetGame();
            }
            if(!this.Board.IsSaved)
                this.SaveState();
            this.CurrentPlayerIndicator.Texture = this.Board.CurrentPlayer.Texture;
            return true;
        }

        public void Render(float dt)
        {
            this.RenderEngine.Render(dt);
        }

        public bool InitFromSavedData()
        {
            try
            {
                XElement e = XElement.Load("GameState.xml");
                var root = e.DescendantsAndSelf().First(x => x.Name == "root");
                var results = root.DescendantsAndSelf().First(x => x.Name == "results").Elements();
                var board = root.Elements().First(x => x.Name == "board");
                var players = board.Elements().First(x => x.Name == "players").Elements();
                int currentPlayerId = Convert.ToInt32(board.Elements().First(x => x.Name == "currentPlayer").Attributes().First(x => x.Name == "id").Value);

                Player.Player.ResetPlayers();
                var playerList = new List<Player.Player>();
                foreach(var player in players)
                {
                    playerList.Add(CreatePlayerFromSavedData(player));
                }

                List<Player.Player> resultsTable = new List<Player.Player>();
                foreach (var result in results)
                    resultsTable.Add(playerList.Find(x => x.Id == Convert.ToInt32(result.Value)));
                
                this.InitBoard(playerList, currentPlayerId);

                this.UpdateResults(resultsTable);

                this.Board.IsSaved = true;
                
                return true;
            }
            catch
            {
                return false;
            }
        }

        private Player.Player CreatePlayerFromSavedData(XElement player)
        {
            Player.Player newPlayer;
            switch (player.Name.ToString())
            {
                case "aiPlayer":
                    int target = Convert.ToInt32(player.Elements().First(x => x.Name == "target").Attributes().First(x => x.Name == "id").Value);
                    newPlayer = new Player.AIPlayer(target);
                    break;
                default:
                    newPlayer = new Player.Player();
                    break;
            }

            var pieces = player.DescendantsAndSelf().Where(x => x.Name == "piece");
            for (int i = 0; i < pieces.Count(); i++)
            {
                int xPos = Convert.ToInt32(pieces.ElementAt(i).DescendantsAndSelf().First(x => x.Name == "x").Value);
                int yPos = Convert.ToInt32(pieces.ElementAt(i).DescendantsAndSelf().First(x => x.Name == "y").Value);
                int zPos = Convert.ToInt32(pieces.ElementAt(i).DescendantsAndSelf().First(x => x.Name == "z").Value);
                newPlayer.Pieces[i].HexCoord = new GameParts.Hex(xPos, yPos, zPos);


                try
                {
                    if (((Player.AIPlayer)newPlayer).GoalPositions.Contains(newPlayer.Pieces[i].HexCoord))
                    {
                        ((Player.AIPlayer)newPlayer).PiecesInPlace.Add(newPlayer.Pieces[i]);
                    }
                }
                catch { }
            }

            return newPlayer;
        }

        public void SaveState()
        {
            this.SaveFileWatcher.EnableRaisingEvents = false;

            var root = new XElement(XName.Get("root"));
            var results = new XElement(XName.Get("results"));
            var board = new XElement(XName.Get("board"));

            foreach(var player in this.LogicEngine.ResultsTable)
            {
                var result = new XElement(XName.Get("result"));
                result.Value = player.Id.ToString();
                results.Add(result);
            }

            var currentPlayerId = new XElement(XName.Get("currentPlayer"));
            XAttribute idAttr = new XAttribute(XName.Get("id"), this.Board.CurrentPlayerId);
            currentPlayerId.ReplaceAttributes(idAttr);
            board.Add(currentPlayerId);

            var players = new XElement(XName.Get("players"));
            board.Add(players);

            foreach (var player in this.Board.Players)
            {
                players.Add(CreatePlayerElement(player));
            }

            root.Add(results);
            root.Add(board);
            root.Save("GameState.xml");

            this.Board.IsSaved = true;

            this.SaveFileWatcher.Path = Environment.CurrentDirectory;

            this.SaveFileWatcher.EnableRaisingEvents = true;
        }

        private XElement CreatePlayerElement(Player.Player player)
        {
            XElement playerElement;
            if (player is Player.AIPlayer)
            {
                playerElement = new XElement(XName.Get("aiPlayer"));
                var targetElement = new XElement(XName.Get("target"));
                XAttribute idAttr = new XAttribute(XName.Get("id"), ((Player.AIPlayer)player).TargetPiece);
                targetElement.ReplaceAttributes(idAttr);
                playerElement.Add(targetElement);
            }
            else
            {
                playerElement = new XElement(XName.Get("player"));
            }

            foreach(var piece in player.Pieces)
            {
                var pieceElement = new XElement(XName.Get("piece"));
                var xElement = new XElement(XName.Get("x"));
                var yElement = new XElement(XName.Get("y"));
                var zElement = new XElement(XName.Get("z"));

                xElement.Value = piece.HexCoord.X.ToString();
                yElement.Value = piece.HexCoord.Y.ToString();
                zElement.Value = piece.HexCoord.Z.ToString();

                pieceElement.Add(xElement);
                pieceElement.Add(yElement);
                pieceElement.Add(zElement);
                playerElement.Add(pieceElement);
            }

            return playerElement;
        }

        private void InitGraphics()
        {
            this.RenderEngine.Add(this.Board.Holes);
            foreach (var player in this.Board.Players)
            {
                this.RenderEngine.Add(player.Pieces);
            }

            this.RenderEngine.Add(this.CurrentPlayerIndicator);
        }

        private void UpdateResults(List<Player.Player> resultsTable)
        {
            this.RenderEngine.Remove(this.ResultsTableObjects);
            int top = 10;
            foreach(var player in resultsTable)
            {
                var tableObject = new GameParts.GameObject(new Vector2(60, top));
                tableObject.Texture = player.Texture;
                this.RenderEngine.Add(tableObject);
                top += 60;
            }
        }
    }
}
