﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Kinaschack.Scenes
{
    interface IScene
    {
        void Initialize();
        void LoadContent(SpriteBatch spriteBatch);
        bool Update(float dt);
        void Render(float dt);
        bool InitFromSavedData();
    }
}
